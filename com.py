#!/usr/bin/python

#common functions and data for pyaddress
#Peter Wolf
#started 5-11-2019

import os
import sys
#import com
#sys.path.append(com.homedir+"/MyPrograms/Python/MyLibraries")

def removeduplicates(thelist):
    newlist=[]
    for el in thelist:
        if not el in newlist:
            newlist.append(el)
    return(newlist)

def normalise(val,lower,upper):
    if val<lower:
        val=lower
    elif val>upper:
        val=upper
    return(val)
version="v0.5"
homedir=os.path.expanduser('~')+'/'
lastfile="lastfile.dat"
recordfields=["Name","Birthdate","Occupation","Address","Phone","Mobile phone","Email address","Website","Gender"]
