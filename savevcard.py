#!/usr/bin/python

#Saves a vcard file given an array from pyaddressbook
#Peter Wolf
#started 5-11-2019


import os
import sys
import com
import comvcard
sys.path.append(com.homedir+"/MyPrograms/Python/MyLibraries")
import subprocess
import time
import listfile as lf
class save(object):
    def __init__(self,filename,ab):
        out=[]
        for record in ab:
            out.append("BEGIN:VCARD")
            for key in record:
                header=comvcard.fieldheaders[com.recordfields.index(key)]
                if header!="":
                    out.append(header+record[key])
            out.append("END:VCARD")
        lf.savelist(filename,out)
