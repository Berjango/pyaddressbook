#! /usr/bin/python
#Some functions to do with using lists in files with one item per line.
#Author - Peter Wolf
#25-3-2018
#freakcycle@optusnet.com.au

import os
import time

def loadlist(filename):
    thelist=[]
    try:
        f=open(filename)
        thelist=f.readlines()
        f.close()
        thelist = [x.strip() for x in thelist]
        while thelist[-1]=="":
            thelist.pop(-1)
    except:
        pass
    return(thelist)

def removeduplicatesinlist(listname):
    arr=loadlist(listname)
    savelistnoduplicates(listname,arr)
    
def removefromlist(listname,item):
    thelist=self.loadlist(listname)
    neolist=list(thelist)
    for el in thelist:
        if el.lower in item.lower():
            neolist.remove(el)
    savelist(listname,neolist)
    return(neolist)

def savelist(listname,thelist):
    f = open(listname, "w")
    for el in thelist:
        f.write(str(el)+"\n")
    f.close()

def savelistnoduplicates(listname,thelist):
    savelist(listname,list(set(thelist)))

def addtolist(listname,item):
    f = open(listname, "a")
    f.write(item+"\n")
    f.close()
    
def inlist(adr,thelist):
    inlist=0
    for el in thelist:
        if el.lower() in adr.lower():
            inlist+=1
    return(inlist)

def backuplist(listname):
    if os.path.isfile(listname):
        os.system("cp "+listname+" "+listname+"backup"+time.strftime("%H:%M:%S"))
