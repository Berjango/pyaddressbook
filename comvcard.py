#!/usr/bin/python

#common functions and data for pyaddress
#Peter Wolf
#started 5-11-2019

import os
import sys

fieldcodes=["FN","BDAY","","ADR","TEL;type=HOME","TEL;type=CELL","@","",""]
altfieldcodes=["","","","","TEL;","","","",""]
fieldheaders=["FN:","BDAY:","","ADR:","TEL;type=HOME:","TEL;type=CELL:","EMAIL:","",""]
