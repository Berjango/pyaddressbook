#!/usr/bin/python

#Saves a ldif file given an array from pyaddressbook
#Peter Wolf
#started 6-11-2019


import os
import sys
import com
import comldif
sys.path.append(com.homedir+"/MyPrograms/Python/MyLibraries")
import subprocess
import time
import listfile as lf
class save(object):
    def __init__(self,filename,ab):
        out=[]
        for record in ab:
            try:
                emailaddress=record["Email address"]
            except:
                emailaddress=""
            firstline="dn: cn="+record["Name"]+",mail="+emailaddress
            out.append(firstline)
            out.append("objectclass: top")
            out.append("objectclass: person")
            out.append("objectclass: organizationalPerson")
            out.append("objectclass: inetOrgPerson")
            out.append("objectclass: mozillaAbPersonAlpha")
            for key in record:
                header=comldif.fieldcodes[com.recordfields.index(key)]
                if header!="":
                    out.append(header+record[key])
            out.append("")
        lf.savelist(filename,out)
