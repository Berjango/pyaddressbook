#!/usr/bin/python

#Converts a ldif file to an array of dictionaries for use with pyaddressbook
#Peter Wolf
#started 5-11-2019


import os
import sys
import com
import comldif
sys.path.append(com.homedir+"/MyPrograms/Python/MyLibraries")
import subprocess
import time
import listfile as lf
class convert(object):
    def __init__(self,filename):
        self.filename=filename
        self.rawdata=lf.loadlist(filename)
        self.ab=[]
        currentrecord={}
        fieldcodes=list(comldif.fieldcodes)
        for line in self.rawdata:
            if "" == line:
                self.ab.append(currentrecord)
                currentrecord={}
                fieldcodes=list(comldif.fieldcodes)
                continue
            for ind,code in enumerate(fieldcodes):
                if code in line and code!="":
                    currentrecord[com.recordfields[fieldcodes.index(code)]]=line.split(":")[-1]
                    fieldcodes[ind]=""
        self.ab.append(currentrecord)
