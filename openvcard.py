#!/usr/bin/python

#Converts a vcard file to an array of dictionaries for use with pyaddressbook
#Peter Wolf
#started 5-11-2019


import os
import sys
import com
import comvcard
sys.path.append(com.homedir+"/MyPrograms/Python/MyLibraries")
import subprocess
import time
import listfile as lf
class convert(object):
    def __init__(self,filename):
        self.filename=filename
        self.rawdata=lf.loadlist(filename)
        self.ab=[]
        currentrecord={}
        fieldcodes=list(comvcard.fieldcodes)
        altfieldcodes=list(comvcard.altfieldcodes)
        for line in self.rawdata:
            if "END:VCARD" in line:
                self.ab.append(currentrecord)
                currentrecord={}
                fieldcodes=list(comvcard.fieldcodes)
                altfieldcodes=list(comvcard.altfieldcodes)
                continue
            for ind,code in enumerate(fieldcodes):
                if code in line and code!="":
                    currentrecord[com.recordfields[fieldcodes.index(code)]]=line.split(":")[-1]
                    fieldcodes[ind]=""
            for ind,code in enumerate(altfieldcodes):
                if code in line and code!="":
                    val=line.split(":")[-1]
                    there=False
                    for key in currentrecord:
                        if currentrecord[key]==val:
                            there=True
                            break
                    if not there:
                        currentrecord[com.recordfields[altfieldcodes.index(code)]]=val
                    altfieldcodes[ind]=""

