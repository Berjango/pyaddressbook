#!/usr/bin/python

#A python based address book for linux
#Peter Wolf
#started 4-11-2019

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,GObject

import os
import sys
import com
#sys.path.append(com.homedir+"/MyPrograms/Python/MyLibraries")
import subprocess
import time
import openvcard
import savevcard
import openldif
import saveldif
import listfile as lf

class PyApp(Gtk.Window):
    def __init__(self):
        self.bookchanged=False
        self.currentrecord={}
        self.index=0
        self.fields=[]
        self.blankrecord={}
        for field in com.recordfields:
            self.blankrecord[field]=""
        self.ab=[self.blankrecord]
        try:
            f=open(com.lastfile,"r")
            self.filename=f.readline()
            f.close()
        except:
            self.filename="blank"
        super(PyApp, self).__init__()

        self.set_title("ADDRESS BOOK  "+com.version)
        self.set_size_request(1100, 500)
        self.set_resizable(False)
        #self.set_position(Gtk.WIN_POS_CENTER)
#        self.connect("visibility_notify_event",self.refreshemailbuttons)
        self.fixed = Gtk.Fixed()
            
        self.button = Gtk.Button("Open")
        self.fixed.put(self.button, 10, 20)
        self.button.connect("clicked",self.open)

        self.button = Gtk.Button("Save")
        self.fixed.put(self.button, 10,60)
        self.button.connect("clicked",self.save)

        self.button = Gtk.Button("Save as")
        self.fixed.put(self.button, 10,100)
        self.button.connect("clicked",self.saveas)

        self.button = Gtk.Button("Add file")
        self.fixed.put(self.button, 10,140)
        self.button.connect("clicked",self.addfile)

        self.button = Gtk.Button("Print")
        self.fixed.put(self.button, 10,180)
        self.button.connect("clicked",self.printbook)


        secondrowyoffset=60
        self.button = Gtk.Button("<<")
        self.fixed.put(self.button, 220,secondrowyoffset)
        self.button.connect("clicked",self.firstrecord)
        self.button = Gtk.Button("<")
        self.fixed.put(self.button, 270,secondrowyoffset)
        self.button.connect("clicked",self.previousrecord)
        self.numrec = Gtk.Label(str(self.index+1))
        self.fixed.put(self.numrec,326,secondrowyoffset+10)
        
        self.button = Gtk.Button(">")
        self.fixed.put(self.button, 360,secondrowyoffset)
        self.button.connect("clicked",self.nextrecord)
        self.button = Gtk.Button(">>")
        self.fixed.put(self.button, 410,secondrowyoffset)
        self.button.connect("clicked",self.lastrecord)

        self.button = Gtk.Button("New record")
        self.fixed.put(self.button, 460,secondrowyoffset)
        self.button.connect("clicked",self.newrec)

        self.button = Gtk.Button("delete")
        self.fixed.put(self.button, 560,secondrowyoffset)
        self.button.connect("clicked",self.delit)

        self.button = Gtk.Button("Search")
        self.fixed.put(self.button, 730,secondrowyoffset)
        self.button.connect("clicked",self.search)
        self.search= Gtk.Entry()
        self.fixed.put(self.search, 800,secondrowyoffset+5)
        self.search.set_width_chars(30)

        self.button = Gtk.Button("Save record")
        self.fixed.put(self.button, 630,secondrowyoffset)
        self.button.connect("clicked",self.saverec)



        self.button = Gtk.Button("About")
        self.fixed.put(self.button, 10,370)
        self.button.connect("clicked",self.about)

        self.button = Gtk.Button("Help")
        self.fixed.put(self.button, 10,405)
        self.button.connect("clicked",self.help)

        self.numrecords = Gtk.Label("Number of records = "+str(len(self.ab)))
        self.fixed.put(self.numrecords,220,30)
        self.filenamelabel = Gtk.Label("Filename = "+self.filename)
        self.fixed.put(self.filenamelabel,220,10)

        self.button = Gtk.Button("Quit")
        self.button.connect("clicked",self.quit)
#        hbox.pack_start(self.button, True, True, 0)
        self.fixed.put(self.button, 10,440)

        for index,field in enumerate(com.recordfields):
            self.recordelements(index)
        self.loadfile(self.filename)



        self.connect("destroy", self.quit)
        self.add(self.fixed)
        self.show_all()
        self.fileformats={"vcf":object,"ldif":object}
        self.filefilters()



    def booktotext(self):
        self.text=[]
        for record in self.ab:
            for key in com.recordfields:
                try:
                    data=record[key]
                except:
                    data=""
                if data.strip()!="":
                    self.text.append(" "+key+"   "+data)
            self.text.append("")

    def printbook(self,e):
        self.booktotext()
        filetoprint="/tmp/addressbook.txt"    
        lf.savelist(filetoprint,self.text)
        command="lp "+filetoprint
        os.system(command)
        
    def quit(self,e):
        if not self.bookchanged:
            Gtk.main_quit()
            return
            
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            (Gtk.STOCK_YES, Gtk.ResponseType.YES,
             Gtk.STOCK_NO, Gtk.ResponseType.NO),"")
        dialog.set_title("Save changes?")
        response=dialog.run()
        dialog.destroy()
        if response == Gtk.ResponseType.YES:
            self.save(0)
        Gtk.main_quit()
        
    def showinfo(self):
        self.numrecords.set_text("Number of records = "+str(len(self.ab)))
        self.filenamelabel.set_text("Filename = "+self.filename)
        self.numrec.set_text(str(self.index+1))

    def recordelements(self,index):
        xoffset=100
        xoffset2=120
        yoffset=120
        elementysize=40
        fieldwidth=100
        self.label = Gtk.Label(com.recordfields[index])
        self.fixed.put(self.label, xoffset, yoffset+elementysize*index)

        self.fields.append( Gtk.Entry())
        self.fixed.put(self.fields[-1], xoffset+xoffset2, yoffset+elementysize*index)
        self.fields[-1].set_width_chars(fieldwidth)
        try:
            self.fields[-1].set_text(self.currentrecord[index])
        except:
            pass


    def showrecord(self,record):
        for index,field in enumerate(com.recordfields):
            try:
                self.fields[index].set_text(record[field])
            except:
                self.fields[index].set_text("")
        self.showinfo()
    def shownewcurrentrecord(self):
        self.index=com.normalise(self.index,0,len(self.ab)-1)
        try:
            self.currentrecord=self.ab[self.index]
        except:
            self.currentrecord=self.ab[self.index]
            
        self.showrecord(self.currentrecord)
    def firstrecord(self,e):
        self.index=0
        self.shownewcurrentrecord()
    def previousrecord(self,e):
        self.index-=1
        self.shownewcurrentrecord()
    def nextrecord(self,e):
        self.index+=1
        self.shownewcurrentrecord()
    def lastrecord(self,e):
        self.index=len(self.ab)-1
        self.shownewcurrentrecord()
    def newrec(self,e):
        self.currentrecord=self.blankrecord
        self.showrecord(self.currentrecord)
        self.ab.insert(self.index,self.currentrecord)
    def search(self,e):
        searchlist=self.ab[self.index+1:]+self.ab[:self.index+1]
        searchtext=self.search.get_text()
        for index,record in enumerate(searchlist):
            for key in record:
                if searchtext in record[key]:
                    self.index+=index+1
                    self.index=self.index % len(self.ab)
                    self.shownewcurrentrecord()
                    return
            
    def saverec(self,e):
        currentrecord={}
        for index,field in enumerate(com.recordfields):
            currentrecord[field]=self.fields[index].get_text()
        self.ab[self.index]=currentrecord
        self.currentrecord=currentrecord
        self.bookchanged=True

    def delit(self,e):
        self.bookchanged=True
        if len(self.ab)==1:
            self.ab[0]=self.blankrecord
        else:
            self.ab[self.index:self.index+1]=[]
        self.shownewcurrentrecord()


    def filefilters(self):
        for fformat in self.fileformats:
            filterobj= Gtk.FileFilter()
            filterobj.set_name(fformat)
            filterobj.add_pattern("*."+fformat)
            self.fileformats[fformat]=filterobj

    def addfile(self,e):
        firstrecords=self.ab
        self.open(0)
        self.ab=firstrecords+self.ab
        self.shownewcurrentrecord()
        self.bookchanged=True

    def open(self,e):
        dialog = Gtk.FileChooserDialog("Please choose a file", self,
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        for fformat in self.fileformats:
            dialog.add_filter(self.fileformats[fformat])
        dialog.set_current_folder(com.homedir)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            print("File selected: " + dialog.get_filename())
            filename=dialog.get_filename()
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")
            dialog.destroy()
            return
        dialog.destroy()
        self.filename=filename
        self.loadfile(filename)

    def loadfile(self,filename):
        extension=filename.split(".")[-1]
        if "vcf"==extension.lower():
            self.ab=openvcard.convert(filename).ab
        elif "ldif"==extension.lower():
            self.ab=openldif.convert(filename).ab
        else:
            return
        self.ab=com.removeduplicates(self.ab)
        self.shownewcurrentrecord()
        self.savefilename()
        self.bookchanged=False

        

    def save(self,e):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            (Gtk.STOCK_YES, Gtk.ResponseType.YES,
             Gtk.STOCK_NO, Gtk.ResponseType.NO),"")
        dialog.set_title("Overwrite "+self.filename+"?")
        response=dialog.run()
        dialog.destroy()
        if response == Gtk.ResponseType.NO:
            return
        print ("SAVING "+self.filename)

        extension=self.filename.split(".")[-1]
        if "vcf"==extension.lower():
            savevcard.save(self.filename,self.ab)
            self.bookchanged=False
        elif "ldif"==extension.lower():
            self.bookchanged=False
            saveldif.save(self.filename,self.ab)
    def saveas(self,e):
        dialog = Gtk.FileChooserDialog("Please choose a file", self,
            Gtk.FileChooserAction.SAVE,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_SAVE, Gtk.ResponseType.OK))

        #self.add_filters(dialog)
        for fformat in self.fileformats:
            dialog.add_filter(self.fileformats[fformat])
        dialog.set_current_folder(com.homedir)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            print("File selected: " + dialog.get_filename())
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")
            dialog.destroy()
            return

        filename=dialog.get_filename()
        dialog.destroy()
        extension=filename.split(".")[-1]
        if "vcf"==extension.lower():
            savevcard.save(filename,self.ab)
        elif "ldif"==extension.lower():
            saveldif.save(filename,self.ab)
        else:
            return
        self.filename=filename
        self.showinfo()
        self.savefilename()
        self.bookchanged=False
        
    def about(self, event):
        text="\npyaddressbook  "+com.version+"\nA simple address book program which can load and save different file formats.\nAuthor - Peter Wolf\nStarted - 4 November 2019\nEmail address - dougalite@gmail.com\nLicense - GNU GPL\n"
        text2="Operating system - Linux Mint 19 MATE,should run on other Linux distributions.\n\nDonations - 1BM9UiWp2xmf8bkqr8o9zgPT8PuZfHZqqF"
     
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK, text+text2)
        dialog.set_title("About")
        dialog.run()
        dialog.destroy()
    def help(self, event):
        text='''
Supported file formats :
    
    .vcf
    .ldif
    
Requirements :
    
- Linux operating system, preferably the Linux Mint 19 MATE distribution.
- Python 2.7 or higher 
- Minimum screen resolution = 1280 x 1024
- P4 computer or better with at least 1GB ram.
'''
     
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK, text)
        dialog.set_title("Help")
        dialog.run()
        dialog.destroy()
    def savefilename(self):
        f=open(com.lastfile,"w")
        f.write(self.filename)
        f.close()


    def __del__(self):
        pass
a=PyApp()
Gtk.main()
